# Comm-net-2022-Routing-Project

This is the repository that contains the assignment of the routing project for the Spring 2022.
This project is part of our [communication networks course](https://comm-net.ethz.ch/) at ETH Zurich. 

The assignment text is available in the **wiki**.


